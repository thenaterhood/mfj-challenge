## Levesque MFJ Code Challenge

### Assumptions
- I wasn't sure how to handle the phrases and punctuated words found in the words.txt file so by default they're ignored. Passing --allow-phrases will use them, with punctuation and whitespace stripped so they can be answers to the puzzle.
- The algorithm I implemented can solve a 2-word sequence in a little under 2 seconds. I imagine, as is the nature of these puzzles, there could be more optimal solutions especially if a larger word list is involved. A larger word list would be a larger search space and will take longer.

### How to build
Implemented on Java 11
./gradlew build

### How to Run

Example command:
`java -jar solver.jar --board="LWC,GTK,ERM,PIA" --allow-phrases --sequence-length=2 --word-file=words.txt`

The following command line options are REQUIRED:
- `--board=[board]` - provide a board definition in the format "LWC,GTK,ERM,PIA". The general expectation is a four-side board with three letters on each side, but boards of any number of sides with any number of letters (including asymmetrical sides) should work.

The following command line options are OPTIONAL:
- `--allow-phrases` - whether to use phrases found in the words.txt file. If omitted, lines in the words file that contain punctuation or whitespace (aside from the newline at the end of the line) will be ignored as possible solutions.
- `--word-file=[words.txt]` - provide a path to a file containing allowable words for the puzzle. Defaults to "words.txt".
- `--sequence-length=[2]` - provide a sequence length to solve the board in. Defaults to 2, but longer sequences should also work.

