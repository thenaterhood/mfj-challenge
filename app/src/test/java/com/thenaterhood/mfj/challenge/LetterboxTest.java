package com.thenaterhood.mfj.challenge;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

/**
 * These are some basic tests for the solver. This is not
 * a 100% coverage situation, these are some basic
 * tests of functionality.
 */
class LetterboxTest {

    private LetterboxBoard board;

    @BeforeEach
    public void setUp() {

        List<LetterboxNode> side1 = new ArrayList<>();
        side1.add(new LetterboxNode('r'));
        side1.add(new LetterboxNode('m'));
        side1.add(new LetterboxNode('e'));

        List<LetterboxNode> side2 = new ArrayList<>();
        side2.add(new LetterboxNode('w'));
        side2.add(new LetterboxNode('c'));
        side2.add(new LetterboxNode('l'));

        List<LetterboxNode> side3 = new ArrayList<>();
        side3.add(new LetterboxNode('k'));
        side3.add(new LetterboxNode('g'));
        side3.add(new LetterboxNode('t'));

        List<LetterboxNode> side4 = new ArrayList<>();
        side4.add(new LetterboxNode('i'));
        side4.add(new LetterboxNode('p'));
        side4.add(new LetterboxNode('a'));

        board = new LetterboxBoard();
        board.addSide(side1);
        board.addSide(side2);
        board.addSide(side3);
        board.addSide(side4);

        board.addWord("wigwam");
        board.addWord("marketplace");
        board.addWord("pragmatic");
        // This is a string that should solve the test board in one word
        board.addWord("rwkimcgpelta");
    }

    @Test
    void testSolveOneWord_OneSolution() {
        // We should find only the sequence of one
        List<List<String>> actual = board.solve(1);

        List<String> expected = new ArrayList<>();
        expected.add("rwkimcgpelta");

        assertEquals(1, actual.size());
        assertEquals(expected, actual.get(0));
    }

    @Test
    void testSolveTwoWords_OneSolution() {
        // We should find only the single sequence of two words
        List<List<String>> actual = board.solve(2);

        List<String> expected = new ArrayList<>();
        expected.add("wigwam");
        expected.add("marketplace");

        assertEquals(1, actual.size());
        assertEquals(expected, actual.get(0));
    }

    @Test
    void testSolveTwoWords_TwoSolutions() {
        // We should find only the two sequences of two words
        board.addWord("cakewalk");
        List<List<String>> actual = board.solve(2);

        List<String> expectedSolution1 = new ArrayList<>();
        expectedSolution1.add("pragmatic");
        expectedSolution1.add("cakewalk");

        List<String> expectedSolution2 = new ArrayList<>();
        expectedSolution2.add("wigwam");
        expectedSolution2.add("marketplace");

        actual.sort(new java.util.Comparator<List<String>>() {
            public int compare(List<String> a, List<String> b) {
                return a.get(0).charAt(0) - b.get(0).charAt(0);
            }});

        assertEquals(2, actual.size());
        assertEquals(expectedSolution1, actual.get(0));
        assertEquals(expectedSolution2, actual.get(1));
    }

    @Test
    void testNormalizeCasing() {
        // Check that if we add a word with caps, we'll still find
        // it as a solution. This is a very similar test to the previous
        // one but we won't bother checking both solutions, just
        // that we got two.
        board.addWord("CaKeWaLk");
        List<List<String>> actual = board.solve(2);

        assertEquals(2, actual.size());
    }
}
