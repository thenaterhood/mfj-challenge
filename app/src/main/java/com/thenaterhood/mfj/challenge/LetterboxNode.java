package com.thenaterhood.mfj.challenge;

import java.util.ArrayList;
import java.util.List;

public class LetterboxNode {

    private List < LetterboxNode > reachable;
    private char letter;

    public LetterboxNode(char letter) {
        this.letter = Character.toLowerCase(letter);
        reachable = new ArrayList < LetterboxNode > ();
    }

    /**
     * Get the nodes (letters) that are reachable from this one
     * @return List<LetterboxNode>
     */
    public List < LetterboxNode > getReachable() {
        return reachable;
    }

    /**
     * Get the letter associated with this node
     * @return char
     */
    public char getLetter() {
        return this.letter;
    }

    /**
     * Add a reachable node (letter) from this node
     * @param node
     */
    public void addReachable(LetterboxNode node) {
        reachable.add(node);
    }
}