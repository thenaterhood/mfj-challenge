package com.thenaterhood.mfj.challenge;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class LetterboxBoard {

    private Set < LetterboxNode > nodes;
    private HashMap < Character, List < String >> wordlist;
    private Set < String > impossibleWords;
    private List<List<String>> solutions;
    private List<Character> validCharacters;

    public LetterboxBoard() {
        this.nodes = new HashSet < > ();
        this.impossibleWords = new HashSet < > ();
        this.validCharacters = new ArrayList<>();
        this.solutions = new ArrayList<>();
        this.wordlist = new HashMap<>();
    }

    /**
     * Add a "side" to the letterbox board. The general
     * expectation for a board is 4 sides with 3 letters ('nodes')
     * but this should support any configuration of board sizes.
     * @param side
     */
    public void addSide(List < LetterboxNode > side) {

        for (LetterboxNode newNode: side) {
            validCharacters.add(newNode.getLetter());
            for (LetterboxNode oldNode: nodes) {
                oldNode.addReachable(newNode);
                newNode.addReachable(oldNode);
            }
        }

        nodes.addAll(side);
    }

    /**
     * Add an individual word to the word list
     * @param word
     */
    public void addWord(String word) {
        word = word.toLowerCase();
        Character firstChar = word.charAt(0);

        // This isn't strictly necessary for the algorithm, but does help
        // shrink the search space and improve performance. If the word has
        // characters that aren't on the board, just ignore it.
        if (!word.chars().mapToObj(c -> (char) c).allMatch(validCharacters::contains)) {
            return;
        }

        List<String> wordListSection = wordlist.get(firstChar);

        if (wordListSection == null) {
            wordListSection = new ArrayList<>();
        }

        wordListSection.add(word);
        wordlist.put(firstChar, wordListSection);
    }

    /**
     * 
     * @param path - path to a word list text file (one word/phrase per line)
     * @param validCharacters - characters allowed by the board configuration
     * @return - a hashmap providing a mapping of first letter to a list of words starting
     *   with that letter for quick lookups of what words could come next given a starting
     *   point.
     */
    public void loadWordlistFromFile(String path, boolean allowPhrases) throws FileNotFoundException {

        wordlist = new HashMap<>();

        // Although I've used a hashmap for this purpose, I also considered a couple
        // other options that could likely lend themselves to different algorithms.
        // - creating a graph representation of the word list where each word would be a node with
        //   connections to words that could follow it. E.g:
        //   node("apple") -> [node("enigma"), node("encryption"), etc]
        //   At the time of writing, I wasn't convinced that this would be significantly
        //   better.
        //
        // - Creating a graph representation of the word list by character, e.g:
        //   node("a") -> node("p") -> node("p") -> node("l") -> node("e")
        //   This could maybe lend itself to an algorithm where the graph could maybe be
        //   overlayed on the board, but adds some challenges such as - in the case
        //   of words that contain each other ("app" contained by "apple") - we would need
        //   some sort of end marker to know when we completed a word. It also might force us
        //   to do a graph traversal for every word we add which might take some time.
        //
        int wc = 0;
        int acceptableWc = 0;

        try {
            Scanner scanner = new Scanner(new File(path));
            while (scanner.hasNextLine()) {
                wc++;

                // Get the next word from the file and normalize its
                // casing.
                String word = scanner.nextLine().toLowerCase();

                // Filter out non-alpha characters if so configured.
                // This helps shrink the search space.
                if (allowPhrases) {
                    word = word.replaceAll("[^A-Za-z]+", "");
                } else if (!word.matches("[A-Za-z]+")) {
                    continue;
                }

                // Filter out any words that are too short
                if (word.length() < 3) {
                    continue;
                }

                addWord(word);
                acceptableWc++;
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            throw e;
        }

        System.out.println(acceptableWc + " usable words loaded of " + wc + " words found");
    }

    /**
     * Print all the solutions to the puzzle
     */
    public List<List<String>> solve(int sequenceLength) {

        solutions = new ArrayList<>();

        // I debated returning an array of them instead but since this
        // is a one-time use, I figured I would save the memory space
        // and print them as they were found.
        for (LetterboxNode n: nodes) {
            getNextWord(n, new ArrayList < > (), new ArrayList < > (), sequenceLength);
        }

        return solutions;
    }

    /**
     * Determines if a discovered path through the board uses all
     * the letters.
     * @param path - the traversal path of nodes
     * @return - whether the path completes the board
     */
    private boolean pathIsComplete(List < LetterboxNode > path) {
        Set < LetterboxNode > uniquePathNodes = new HashSet < > ();

        for (LetterboxNode node: path) {
            uniquePathNodes.add(node);
        }

        return uniquePathNodes.equals(nodes);
    }

    /**
     * Adds a word to the discovered sequence from a given starting
     * point. If the word found is the last in the sequence (based on
     * the length desired for a sequence), check if the path is complete
     * and if so, print the solution to STDOUT.
     * @param node
     * @param path
     * @param foundWords
     */
    private void getNextWord(LetterboxNode node, List < LetterboxNode > path, List < String > foundWords, int sequenceLength) {

        if (foundWords.size() >= sequenceLength) {
            System.out.println(foundWords);

            if (pathIsComplete(path)) {
                solutions.add(foundWords);
            }

            return;
        }

        List<String> wordListSection = wordlist.get(node.getLetter());
        if (wordListSection == null) {
            return;
        }

        for (String word: wordListSection) {

            List < LetterboxNode > newPath = new ArrayList < > ();
            newPath.addAll(path);
            List < String > words = new ArrayList < > ();
            words.addAll(foundWords);

            if (!impossibleWords.contains(word) && findWord(word, node, newPath)) {
                LetterboxNode nextNode = newPath.get(newPath.size() - 1);
                words.add(word);
                // sequenceLength is intentionally passed unmodified
                getNextWord(nextNode, newPath, words, sequenceLength);
            } else {
                // This is a tiny optimization to the algorithm where if we encounter
                // a word that can't exist on the board during our solution search, we
                // won't search for it again in future sequences.
                // The impact of this compounds if we look for solutions with more words
                // because we shrink our search space as we traverse the board.
                impossibleWords.add(word);
            }
        }
    }

    /**
     * Searches for a word on the board and returns whether there is a possible
     * path to find it.
     * @param word - the word to search for
     * @param node - the starting point on the board
     * @param path - the previous traversal path (generally an empty list when invoking this method)
     * @return
     */
    private boolean findWord(String word, LetterboxNode node, List < LetterboxNode > path) {

        // We're looking for each letter in the word, then as we find it
        // search for the remaining characters in the words.
        // apple
        // pple
        // ple
        // le
        // e
        // If we have no characters left in the word, then we've found it.
        // This works because we filter out words smaller than the rules allow
        // so we shouldn't encounter any "word" that has a length of 0 for
        // any other reason.
        if (word.length() == 0) {
            return true;
        }

        // If the node's letter doesn't match the letter we're looking for,
        // this is the wrong path.
        if (node.getLetter() != word.charAt(0)) {
            return false;
        }

        // Otherwise, add the node to the path and continue on
        path.add(node);

        for (LetterboxNode reachableNode: node.getReachable()) {
            if (findWord(word.substring(1), reachableNode, path)) {
                return true;
            };
        }

        // Since we return true earlier if we found the word, we
        // get here if we've exhausted all possible paths through the board
        // for this work, so the word doesn't work.
        return false;
    }
}