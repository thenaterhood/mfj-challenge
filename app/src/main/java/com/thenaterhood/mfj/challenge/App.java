package com.thenaterhood.mfj.challenge;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        String board = "";
        String wordFile = "words.txt";
        boolean allowPhrases = false;
        int sequenceLength = 2;

        // I would generally use a library for argument parsing but I just wanted
        // something minimal rather than hardcoding parameters.
        for (String arg: args) {

            String[] parameter = arg.split("=");

            switch (parameter[0]) {
                case "--board":
                    // The board definition with each side in comma
                    // separated format. E.g. RME,WCL,KGT,IPA
                    if (parameter.length < 2) {
                        System.out.println("missing parameter to --board");
                        return;
                    }
                    board = parameter[1];
                    break;
                case "--allow-phrases":
                    // Whether phrases are allowed to solve the puzzle.
                    // When passed, whitespace and punctuation in the words
                    // file is ignored.
                    allowPhrases = true;
                    break;
                case "--sequence-length":
                    if (parameter.length < 2) {
                        System.out.println("missing parameter to --sequence-length");
                        return;
                    }
                    // How long of a sequence to solve for. Default is 2.
                    try {
                        sequenceLength = Integer.parseInt(parameter[1]);
                    } catch (NumberFormatException e) {
                        System.out.println("Sequence length must be an integer. Got '" + parameter[1] + "'");
                        return;
                    }
                    break;
                case "--word-file":
                    if (parameter.length < 2) {
                        System.out.println("missing parameter to --word-file");
                        return;
                    }
                    wordFile = parameter[1];
                    break;

                default:
                    System.out.println("Unknown option '" + parameter[0] + "'");
                    return;
            }

        }

        if (board.length() < 1) {
            System.out.println("missing board (pass '--board=RME,WCL,KGT,IPA') or another board");
            return;
        }

        String[] sideDefinitions = board.split(",");
        List < Character > validCharacters = new ArrayList < > ();

        LetterboxBoard letterbox = new LetterboxBoard();

        for (String string: sideDefinitions) {
            ArrayList < LetterboxNode > side = new ArrayList < LetterboxNode > ();

            for (int i = 0; i < string.length(); ++i) {
                Character c = string.charAt(i);
                side.add(new LetterboxNode(c));
                validCharacters.add(c);
            }

            letterbox.addSide(side);
        }

        try {
            letterbox.loadWordlistFromFile(wordFile, allowPhrases);
        } catch (FileNotFoundException e) {
            System.out.println("Failed to load word list");
            return;
        }

        System.out.println(String.format("Finding sequences of length %d on board '%s', using words file '%s'...", sequenceLength, board, wordFile));

        List<List<String>> solutions = letterbox.solve(sequenceLength);
        for (List<String> solution : solutions) {
            System.out.println("Solution: " + solution.toString());
        }
    }
}
